#include <student/gpu.h>

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>


/// \addtogroup gpu_side Implementace vykreslovacího řetězce - vykreslování trojúhelníků
/// @{

// Podělí koordináty trojuhelníka do rozsahu -1 až 1
// Snad GOOD
void perspective_division(Vec4*const ndc,Vec4 const*const p)
{
  for(int a = 0; a < 3 ; ++a)
  {
    ndc->data[a] = p->data[a]/p->data[3];
  }
  ndc->data[3] = p->data[3];
}

// Rozdáhne trojuhelník do rozlišení obrazovky
// Snad GOOD
Vec4 view_port_transformation(Vec4 const&p ,uint32_t width,uint32_t height)
{
  Vec4 res;
  res.data[0] = (p.data[0]*.5f+.5f)*width;
  res.data[1] = (p.data[1]*.5f+.5f)*height;
  res.data[2] = p.data[2];
  res.data[3] = p.data[3];
  return res;
}

// Kopírování artibutů ze čtecí hlavy když je povolená
// Snad GOOD
void reading_head(GPU const*const gpu, GPUAttribute*const att, GPUVertexPullerHead const*const head, uint64_t vertexId)
{
  if(!head->enabled)return;
  GPUBuffer const *const buf = gpu_getBuffer(gpu,head->bufferId);
  uint8_t const *ptr = (uint8_t*)buf->data;
  uint64_t const offset = (uint32_t)head->offset;
  uint64_t const stride = (uint32_t)head->stride;
  uint32_t const size   = (uint32_t)head->type;
  memcpy(att->data,ptr+offset + vertexId*stride,size);
}

// Vytáhnutí vertexů z bufferů
// Snad GOOD
void vertex_puller(GPUInVertex *const inVertex, GPUVertexPuller const *const vao, GPU const *const gpu, uint32_t vertexSHaderInvocation)
{
  // Index inVertexu
  if (gpu_isBuffer(gpu, vao->indices.bufferId))
  {
    GPUBuffer const *const indices_buffer = gpu_getBuffer(gpu, vao->indices.bufferId);

    // Co za typ to je
    if (vao->indices.type == UINT8)
    {
      //8bit
      uint8_t *vertex_id_data = (uint8_t *) indices_buffer->data;
      uint8_t vertex_id = vertex_id_data[vertexSHaderInvocation];
      inVertex->gl_VertexID = vertex_id;
      // Překopírování atributů ze čtecích hlav
      for (unsigned i = 0; i < MAX_ATTRIBUTES; i++)
      {
        reading_head(gpu, inVertex->attributes+i,vao->heads+i, vertex_id);
      }
    }
    else if (vao->indices.type == UINT16)
    {
      // 16bit
      uint16_t *vertex_id_data = (uint16_t *) indices_buffer->data;
      uint16_t vertex_id = vertex_id_data[vertexSHaderInvocation];
      inVertex->gl_VertexID = vertex_id;

      // Překopírování atributů ze čtecích hlav
      for (unsigned i = 0; i < MAX_ATTRIBUTES; i++)
      {
        reading_head(gpu, inVertex->attributes+i,vao->heads+i, vertex_id);
      }
    }
    else
    {
      // 32bit
      uint32_t *vertex_id_data = (uint32_t *) indices_buffer->data;
      uint32_t vertex_id = vertex_id_data[vertexSHaderInvocation];
      inVertex->gl_VertexID = vertex_id;

      // Překopírování atributů ze čtecích hlav
      for (unsigned i = 0; i < MAX_ATTRIBUTES; i++)
      {
        reading_head(gpu, inVertex->attributes+i,vao->heads+i, vertex_id);
      }
    }
  }
  else
  {
    uint32_t vertex_id = vertexSHaderInvocation;
    inVertex->gl_VertexID = vertex_id;

    // Překopírování atributů ze čtecích hlav
    for (unsigned i = 0; i < MAX_ATTRIBUTES; i++)
    {
      reading_head(gpu, inVertex->attributes+i,vao->heads+i, vertex_id);
    }
  }
}

// Snad GOOD
void per_fragment_operation(GPUOutFragment const*const outFragment,GPU*const gpu,Vec4 coord, float depth)
{
  
  GPUFramebuffer*const frame = &gpu->framebuffer;

  // Kontrola polohy fragmentu
  if(coord.data[0] < 0 || coord.data[0] >= frame->width)return;
  if(coord.data[1] < 0 || coord.data[1] >= frame->height)return;
  if(isnan(coord.data[0]))return;
  if(isnan(coord.data[1]))return;

  uint32_t const pixCoord = frame->width*(int)coord.data[1]+(int)coord.data[0];

  // Deph test 
  if(frame->depth[pixCoord] < depth)
  {
    return;
  }
  else
  {
    frame->depth[pixCoord]=depth;
  }

  // Kontlola jestli je barva ve správném rozsahu
  Vec4 color;
  for (int i = 0; i < 4; i++)
  {
    if (outFragment->gl_FragColor.data[i] < 0.0)
    {
      color.data[i]=0.0;
    }
    else if (outFragment->gl_FragColor.data[i] > 1.0)
    {
      color.data[i]=1.0;
    }
    else
    {
      color.data[i]=outFragment->gl_FragColor.data[i];
    }
  }

  frame->color[pixCoord] = color;
}

#define MIN(a, b)       (((a) < (b)) ? (a) : (b))
#define MAX(a, b)       (((a) > (b)) ? (a) : (b))
#define ROUND_UP(x)        ((int)((x) + 1.0f))
#define ROUND_DOWN(x)        ((int)((x)))

/**
 * @brief This function should draw triangles
 *
 * @param gpu gpu 
 * @param nofVertices number of vertices
 */
void gpu_drawTriangles(GPU *const gpu, uint32_t nofVertices)
{

  /// \todo Naimplementujte vykreslování trojúhelníků.
  /// nofVertices - počet vrcholů
  /// gpu - data na grafické kartě
  /// Vašim úkolem je naimplementovat chování grafické karty.
  /// Úkol je složen:
  /// 1. z implementace Vertex Pulleru
  /// 2. zavolání vertex shaderu pro každý vrchol
  /// 3. rasterizace
  /// 4. zavolání fragment shaderu pro každý fragment
  /// 5. zavolání per fragment operací nad fragmenty (depth test, zápis barvy a hloubky)
  /// Více v připojeném videu.
  GPUProgram      const* prg = gpu_getActiveProgram(gpu);
  GPUVertexPuller const* vao = gpu_getActivePuller (gpu);

  GPUVertexShaderData   vd1;
  GPUVertexShaderData   vd2;
  GPUVertexShaderData   vd3;

  GPUFragmentShaderData fd;

  vd1.uniforms = &prg->uniforms;
  vd2.uniforms = &prg->uniforms;
  vd3.uniforms = &prg->uniforms;

  fd.uniforms = &prg->uniforms;

  for(uint32_t v = 0; v < nofVertices; v+=3)
  {
    // Vytáhnutí tří vertexů po sobě a prohnání vertex shaderem
    vertex_puller(&vd1.inVertex,vao,gpu,v);
    prg->vertexShader(&vd1);
    vertex_puller(&vd2.inVertex,vao,gpu,v+1);
    prg->vertexShader(&vd2);
    vertex_puller(&vd3.inVertex,vao,gpu,v+2);
    prg->vertexShader(&vd3);

    // Mám 3 vrcholy na trojůhelník

    // Perspektivní dělení pro tři vrcholi trujuhelníka
    Vec4 ndc1;
    Vec4 ndc2;
    Vec4 ndc3;
    perspective_division(&ndc1, &vd1.outVertex.gl_Position);
    perspective_division(&ndc2, &vd2.outVertex.gl_Position);
    perspective_division(&ndc3, &vd3.outVertex.gl_Position);

    // View port transformace
    Vec4 vp1 = view_port_transformation(ndc1, gpu->framebuffer.width, gpu->framebuffer.height);
    Vec4 vp2 = view_port_transformation(ndc2, gpu->framebuffer.width, gpu->framebuffer.height);
    Vec4 vp3 = view_port_transformation(ndc3, gpu->framebuffer.width, gpu->framebuffer.height);

    ////////////// Tvoření fragmentů /////////////////////

    float minX, maxX, minY, maxY;
    // Nalezeni obalky (minX, maxX), (minY, maxY) trojuhleniku.
	  minX=MIN(MIN(vp1.data[0], vp2.data[0]), vp3.data[0]);
	  maxX=MAX(MAX(vp1.data[0], vp2.data[0]), vp3.data[0]);
	  minY=MIN(MIN(vp1.data[1], vp2.data[1]), vp3.data[1]);
	  maxY=MAX(MAX(vp1.data[1], vp2.data[1]), vp3.data[1]);
    // Oříznuti obalky (minX, maxX, minY, maxY) trojuhleniku podle rozmeru okna.
 	  minX = MAX(0, minX);
	  minY = MAX(0, minY);
	  maxX = MIN(gpu->framebuffer.width - 1, maxX);
	  maxY = MIN(gpu->framebuffer.height - 1, maxY);

    minX = ROUND_DOWN(minX);
    minY = ROUND_DOWN(minY);
    maxX = ROUND_UP(maxX);
    maxY = ROUND_UP(maxY);

    // Vyplnovani: Cyklus pres vsechny body (x, y) v obdelniku (minX, minY), (maxX, maxY).
    // Pro aktualizaci hodnot hranove funkce v bode P (x +/- 1, y) nebo P (x, y +/- 1)
    // vyuzijte hodnoty hranove funkce E (x, y) z bodu P (x, y).
	  for (unsigned y = minY; y <= maxY; y++)
	  {
	  	for (unsigned x = minX; x <= maxX; x++)
	  	{
        Vec4 mid;
        mid.data[0] = x+0.5f;
        mid.data[1] = y+0.5f;
        float edge1 = (mid.data[0] - vp2.data[0]) * (vp3.data[1] - vp2.data[1]) - (mid.data[1] - vp2.data[1]) * (vp3.data[0] - vp2.data[0]);
        float edge2 = (mid.data[0] - vp3.data[0]) * (vp1.data[1] - vp3.data[1]) - (mid.data[1] - vp3.data[1]) * (vp1.data[0] - vp3.data[0]);
        float edge3 = (mid.data[0] - vp1.data[0]) * (vp2.data[1] - vp1.data[1]) - (mid.data[1] - vp1.data[1]) * (vp2.data[0] - vp1.data[0]);
        
	  		if (edge1 <= 0.0f && edge2 <= 0.0f && edge3 <= 0.0f)
	  		{
          // Interpolace atributů
          float area = (vp2.data[1]-vp3.data[1])*(vp1.data[0]-vp3.data[0])+(vp3.data[0]-vp2.data[0])*(vp1.data[1]-vp3.data[1]);
          float lambda1 = ((vp2.data[1]-vp3.data[1])*(mid.data[0]-vp3.data[0])+(vp3.data[0]-vp2.data[0])*(mid.data[1]-vp3.data[1]))/ area;
          float lambda2 = ((vp3.data[1]-vp1.data[1])*(mid.data[0]-vp3.data[0])+(vp1.data[0]-vp3.data[0])*(mid.data[1]-vp3.data[1]))/ area;
          float lambda3 = 1.0f - lambda1 - lambda2;

          for (int i = 0; i < MAX_ATTRIBUTES; i++)
          {
            if(prg->vs2fsType[i] == ATTRIBUTE_VEC4)
            {
              for (int j = 0; j < sizeof(Vec4); j+=4)
              {
                float first = *((float*)&vd1.outVertex.attributes[i].data[j]);
                float second = *((float*)&vd2.outVertex.attributes[i].data[j]);;
                float third = *((float*)&vd3.outVertex.attributes[i].data[j]);;
                *((float*)&fd.inFragment.attributes[i].data[j]) = (first*lambda1/vp1.data[3] + second*lambda2/vp2.data[3] + third*lambda3/vp3.data[3]) / (lambda1/vp1.data[3] + lambda2/vp2.data[3] + lambda3/vp3.data[3]);
              }
            }
            else if (prg->vs2fsType[i] == ATTRIBUTE_VEC3)
            {
              for (int j = 0; j < sizeof(Vec3); j+=4)
              {
                float first = *((float*)&vd1.outVertex.attributes[i].data[j]);
                float second = *((float*)&vd2.outVertex.attributes[i].data[j]);;
                float third = *((float*)&vd3.outVertex.attributes[i].data[j]);;
                *((float*)&fd.inFragment.attributes[i].data[j]) = (first*lambda1/vp1.data[3] + second*lambda2/vp2.data[3] + third*lambda3/vp3.data[3]) / (lambda1/vp1.data[3] + lambda2/vp2.data[3] + lambda3/vp3.data[3]);
              }
            }
            else if (prg->vs2fsType[i] == ATTRIBUTE_VEC2)
            {
              for (int j = 0; j < sizeof(Vec2); j+=4)
              {
                float first = *((float*)&vd1.outVertex.attributes[i].data[j]);
                float second = *((float*)&vd2.outVertex.attributes[i].data[j]);;
                float third = *((float*)&vd3.outVertex.attributes[i].data[j]);;
                *((float*)&fd.inFragment.attributes[i].data[j]) = (first*lambda1/vp1.data[3] + second*lambda2/vp2.data[3] + third*lambda3/vp3.data[3]) / (lambda1/vp1.data[3] + lambda2/vp2.data[3] + lambda3/vp3.data[3]);
              }
            }
            else if (prg->vs2fsType[i] == ATTRIBUTE_FLOAT)
            {
              for (int j = 0; j < sizeof(float); j+=4)
              {
                float first = *((float*)&vd1.outVertex.attributes[i].data[j]);
                float second = *((float*)&vd2.outVertex.attributes[i].data[j]);;
                float third = *((float*)&vd3.outVertex.attributes[i].data[j]);;
                *((float*)&fd.inFragment.attributes[i].data[j]) = (first*lambda1/vp1.data[3] + second*lambda2/vp2.data[3] + third*lambda3/vp3.data[3]) / (lambda1/vp1.data[3] + lambda2/vp2.data[3] + lambda3/vp3.data[3]);
              }
            }
          }

          fd.inFragment.gl_FragCoord.data[0] = mid.data[0];
          fd.inFragment.gl_FragCoord.data[1] = mid.data[1];
          fd.inFragment.gl_FragCoord.data[2] = (vp1.data[2]*lambda1/vp1.data[3] + vp2.data[2]*lambda2/vp2.data[3] + vp3.data[2]*lambda3/vp3.data[3]) / (lambda1/vp1.data[3] + lambda2/vp2.data[3] + lambda3/vp3.data[3]);
          fd.inFragment.gl_FragCoord.data[3] = vp1.data[3];

	  			// Fragment shader
          prg->fragmentShader(&fd);

          // Deph test fragmentu
          per_fragment_operation(&fd.outFragment, gpu, fd.inFragment.gl_FragCoord, fd.inFragment.gl_FragCoord.data[2]);
	  		}
	  	}
	  }
  }
}

/// @}
