#include <student/emptyMethod.h>
#include <student/application.h>
#include <student/cpu.h>
#include <student/globals.h>
#include <student/bunny.h>

/// \addtogroup shader_side Úkoly v shaderech
/// @{

/**
 * @brief This function represents vertex shader of phong method.
 *
 * @param data vertex shader data
 */
void phong_VS(GPUVertexShaderData*const data){
  /// \todo Naimplementujte vertex shader, který transformuje vstupní vrcholy do
  /// clip-space.<br>
  /// <b>Vstupy:</b><br>
  /// Vstupní vrchol by měl v nultém atributu obsahovat pozici vrcholu ve
  /// world-space (vec3) a v prvním
  /// atributu obsahovat normálu vrcholu ve world-space (vec3).<br>
  /// <b>Výstupy:</b><br>
  /// Výstupní vrchol by měl v nultém atributu obsahovat pozici vrcholu (vec3)
  /// ve world-space a v prvním
  /// atributu obsahovat normálu vrcholu ve world-space (vec3).
  /// Výstupní vrchol obsahuje pozici a normálu vrcholu proto, že chceme počítat
  /// osvětlení ve world-space ve fragment shaderu.<br>
  /// <b>Uniformy:</b><br>
  /// Vertex shader by měl pro transformaci využít uniformní proměnné obsahující
  /// view a projekční matici.
  /// View matici čtěte z nulté uniformní proměnné a projekční matici
  /// čtěte z první uniformní proměnné.
  /// <br>
  /// Využijte vektorové a maticové funkce.
  /// Nepředávajte si data do shaderu pomocí globálních proměnných.
  /// Vrchol v clip-space by měl být zapsán do proměnné gl_Position ve výstupní
  /// struktuře.

  ////// Snad GOOD ////////
  Vec3 *position = (Vec3*)data->inVertex.attributes[0].data;
  Vec3 *normal = (Vec3*)data->inVertex.attributes[1].data;
  Mat4 *viewMatrix       = (Mat4*)data->uniforms->uniform[0].data;
  Mat4 *projectionMatrix = (Mat4*)data->uniforms->uniform[1].data;
 
  Mat4 mvp;
  multiply_Mat4_Mat4(&mvp, projectionMatrix, viewMatrix);

  // Výpočet polohy ve clip space
  Vec4 pos_vec4;
  copy_Vec3Float_To_Vec4(&pos_vec4, position, 1.0f);

  Vec4 vPos;
  multiply_Mat4_Vec4(&vPos, &mvp, &pos_vec4);

  copy_Vec4(&data->outVertex.gl_Position, &vPos);

  // Pozice a normála ve world space
  copy_Vec3((Vec3*)data->outVertex.attributes[0].data, position);
  copy_Vec3((Vec3*)data->outVertex.attributes[1].data, normal);
}

/**
 * @brief This function represents fragment shader of phong method.
 *
 * @param data fragment shader data
 */
void phong_FS(GPUFragmentShaderData*const data){
  /// \todo Naimplementujte fragment shader, který počítá phongův osvětlovací
  /// model s phongovým stínováním.<br>
  /// <b>Vstup:</b><br>
  /// Vstupní fragment by měl v nultém fragment atributu obsahovat
  /// interpolovanou pozici ve world-space a v prvním
  /// fragment atributu obsahovat interpolovanou normálu ve world-space.<br>
  /// <b>Výstup:</b><br>
  /// Barvu zapište do proměnné gl_FragColor ve výstupní struktuře.<br>
  /// <b>Uniformy:</b><br>
  /// Pozici kamery přečtěte z uniformní 3 a pozici
  /// světla přečtěte z uniformní 2.
  /// <br>
  /// Dejte si pozor na velikost normálového vektoru, při lineární interpolaci v
  /// rasterizaci může dojít ke zkrácení.
  /// Zapište barvu do proměnné color ve výstupní struktuře.
  /// Shininess faktor nastavte na 40.f
  /// Difuzní barvu materiálu nastavte podle normály povrchu.
  /// V případě, že normála směřuje kolmo vzhůru je difuzní barva čistě bílá.
  /// V případě, že normála směřuje vodorovně nebo dolů je difuzní barva čiště zelená.
  /// Difuzní barvu spočtěte lineární interpolací zelené a bíle barvy pomocí interpolačního parameteru t.
  /// Interpolační parameter t spočtěte z y komponenty normály pomocí t = y*y (samozřejmě s ohledem na negativní čísla).
  /// Spekulární barvu materiálu nastavte na čistou bílou.
  /// Barvu světla nastavte na bílou.
  /// Nepoužívejte ambientní světlo.<br>

  Vec3 *position = (Vec3*)data->inFragment.attributes[0].data;
  Vec3 *normal = (Vec3*)data->inFragment.attributes[1].data;
  Vec3 *light = (Vec3*)data->uniforms->uniform[2].data;
  Vec3 *camera = (Vec3*)data->uniforms->uniform[3].data;

  Vec3 vector_to_camera;
  Vec3 vector_to_light;
  Vec3 incident_vector;
  Vec3 reflect_light;
  
  sub_Vec3(&vector_to_camera, camera, position);
  sub_Vec3(&vector_to_light, light, position);
  sub_Vec3(&incident_vector, position, light);

  normalize_Vec3(&incident_vector, &incident_vector);

  reflect(&reflect_light, &incident_vector, normal);

  normalize_Vec3(&vector_to_camera, &vector_to_camera);
  normalize_Vec3(&vector_to_light, &vector_to_light);
  normalize_Vec3(normal, normal);
  normalize_Vec3(&reflect_light, &reflect_light);

  Vec4 white;
  Vec4 green;
  init_Vec4(&white, 1.0f, 1.0f, 1.0f, 1.0f);
  init_Vec4(&green, 0.0f, 1.0f, 0.0f, 1.0f);

  Vec4 material_specular_color;
  init_Vec4(&material_specular_color, 1.0f, 1.0f, 1.0f, 1.0f);

  Vec4 difuse_color;
  Vec4 specular_color;
  Vec4 output_color;

  // Difůzní barva materiálu
  Vec4 material_difuse_color;
  float t = normal->data[1] * normal->data[1];
  if (t < 0.0f)
  {
    t = 0.0f;
  }
  else if (t > 1.0f)
  {
    t = 1.0f;
  }
    
  mix_Vec4(&material_difuse_color, &green, &white, t);

  
  // Výpočet dyfůzní složky výstupní barvy
  float angle1 = dot_Vec3(normal, &vector_to_light);
  if (angle1 < 0.0f)
  {
    angle1 = 0.0f;
  }
  else if (angle1 > 1.0f)
  {
    angle1 = 1.0f;
  }
  
  multiply_Vec4_Float(&difuse_color, &material_difuse_color, angle1);
  //  Dyfůzní složka světla je 1, takže nemusím nic násobyt

  // Výpočet spekulární složky výstupní barvy
  float angle2 = dot_Vec3(&reflect_light, &vector_to_camera);
  if (angle2 < 0.0f)
  {
    angle2 = 0.0f;
  }
  else if (angle2 > 1.0f)
  {
    angle2 = 1.0f;
  }

  angle2 = pow(angle2, 40.f);
  multiply_Vec4_Float(&specular_color, &material_specular_color, angle2);

  // Výpis finální barvy
  add_Vec4(&output_color, &difuse_color, &specular_color);
  copy_Vec4(&data->outFragment.gl_FragColor, &output_color);
}

/// @}

/// \addtogroup cpu_side Úkoly v cpu části
/// @{

/**
 * @brief This struct holds all variables of phong method.
 */
struct PhongData
{
  ProgramID prg;
  VertexPullerID vao;
  BufferID vertexes;
  BufferID indices;
}phongData;///< this variable holds all data for phong method

/**
 * @brief This function initializes phong method.
 *
 * @param a data for initialization
 */
void phong_onInit(void*a){
  GPU*gpu = (GPU*)a;
  Vec4 clear;
  init_Vec4(&clear,.1f,.1f,.1f,1.f);
  cpu_clearColor(gpu,clear);
  cpu_clearDepth(gpu,1.f);

/// \todo Doprogramujte inicializační funkci.
/// Zde byste měli vytvořit buffery na GPU, nahrát data do bufferů, vytvořit
/// vertex puller a správně jej nakonfigurovat, vytvořit program, připojit k
/// němu shadery a nastavit atributy, které se posílají mezi vs a fs.
/// Do bufferů nahrajte vrcholy králička (pozice, normály) a indexy na vrcholy
/// ze souboru bunny.h.
/// Shader program by měl odkazovat na funkce/shadery phong_VS a phong_FS.
/// V konfiguraci vertex pulleru nastavte dvě čtecí hlavy.
/// Jednu pro pozice vrcholů a druhou pro normály vrcholů.
/// Nultý vertex/fragment atribut by měl obsahovat pozici vertexu.
/// První vertex/fragment atribut by měl obsahovat normálu vertexu.
/// Nastavte, které atributy (jaký typ) se posílají z vertex shaderu do fragment shaderu.
/// <b>Seznam funkcí, které jistě využijete:</b>
///  - cpu_createBuffer()
///  - cpu_bufferData()
///  - cpu_createVertexPuller()
///  - cpu_setVertexPuller()
///  - cpu_enableVertexPullerHead()
///  - cpu_setVertexPullerIndexing()
///  - cpu_createProgram()
///  - cpu_attachShaders()
///  - cpu_setVS2FSType()

  // Buffer vertexů a normál
  phongData.vertexes = cpu_createBuffer(gpu);
  cpu_bufferData(gpu, phongData.vertexes, sizeof(bunnyVertices), (void *)bunnyVertices);

  // Buffer indexů
  phongData.indices = cpu_createBuffer(gpu);
  cpu_bufferData(gpu, phongData.indices, sizeof(bunnyIndices), (void *) bunnyIndices);

  // Nastavení vertex pulleru
  phongData.vao = cpu_createVertexPuller(gpu);

  // Vrcholy 
  cpu_setVertexPuller(gpu, phongData.vao, 0, ATTRIBUTE_VEC3, sizeof(struct BunnyVertex), 0, phongData.vertexes);
  cpu_enableVertexPullerHead(gpu, phongData.vao, 0);

  // Normály
  cpu_setVertexPuller(gpu, phongData.vao, 1, ATTRIBUTE_VEC3, sizeof(struct BunnyVertex), sizeof(Vec3), phongData.vertexes);
  cpu_enableVertexPullerHead(gpu, phongData.vao, 1);

  // Indexace
  cpu_setVertexPullerIndexing(gpu, phongData.vao, UINT32, phongData.indices);

  phongData.prg = cpu_createProgram(gpu);
  cpu_attachShaders(gpu, phongData.prg, phong_VS, phong_FS);

  cpu_setVS2FSType(gpu, phongData.prg, 0, ATTRIBUTE_VEC3);
  cpu_setVS2FSType(gpu, phongData.prg, 1, ATTRIBUTE_VEC3);
}

/**
 * @brief This function draws phong method.
 *
 * @param a data
 */
void phong_onDraw(void*a){
  GPU*gpu = (GPU*)a;
  cpu_clear(gpu);

/// \todo Doprogramujte kreslící funkci.
/// Zde byste měli aktivovat shader program, aktivovat vertex puller, nahrát
/// data do uniformních proměnných a
/// vykreslit trojúhelníky pomocí funkce cpu_drawTriangles.
/// Data pro uniformní proměnné naleznete v externích globálních proměnnénych
/// viewMatrix, projectionMatrix, cameraPosition a lightPosition
/// <b>Seznam funkcí, které jistě využijete:</b>
///  - cpu_useProgram()
///  - cpu_bindVertexPuller()
///  - cpu_uniformMatrix4f()
///  - cpu_uniform3f()
///  - cpu_drawTriangles()
///  - cpu_unbindVertexPuller

  // Snad GOOD
  cpu_bindVertexPuller(gpu, phongData.vao);
  cpu_useProgram(gpu, phongData.prg);

  // Nastavení uniformních proměnných
  cpu_programUniformMatrix4f(gpu, phongData.prg, 0, viewMatrix);
  cpu_programUniformMatrix4f(gpu, phongData.prg, 1, projectionMatrix);
  cpu_programUniform3f(gpu, phongData.prg, 2, lightPosition);
  cpu_programUniform3f(gpu, phongData.prg, 3, cameraPosition);

  cpu_drawTriangles(gpu, sizeof(bunnyIndices)/sizeof(VertexIndex));

  cpu_unbindVertexPuller(gpu);
}

/**
 * @brief This functions frees all the phong data.
 *
 * @param a data
 */
void phong_onExit(void*a){
  GPU*gpu = (GPU*)a;
 
  // Odalokace
  // Snad GOOD
  cpu_deleteProgram(gpu, phongData.prg);
  cpu_deleteVertexPuller(gpu, phongData.vao);
  cpu_deleteBuffer(gpu, phongData.vertexes);
  cpu_deleteBuffer(gpu, phongData.indices);
}

/// @}
